# Matomo Server

## Setup

- git clone the above repository to a server
- create the .env file and set the secrets (copy .env-example)
- Install docker and docker-compose and execute `docker-compose up -d`
